package com.example.drawapp01;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;

import com.google.android.material.bottomnavigation.BottomNavigationItemView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private DrawView drawView;
    private Button undo;
    private Button clear;
    private SeekBar sb;
    private float width;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        drawView = findViewById(R.id.drawView);
        undo = findViewById(R.id.button1);
        clear = findViewById(R.id.button2);
        sb = findViewById(R.id.seekBar1);

        undo.setOnClickListener(this);
        clear.setOnClickListener(this);
        sb.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                float f = 1.0F;
                width = progress/f;

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                drawView.setWidth(width);
            }
        });
    }


    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.button1:
                drawView.backCanvas();
                break;
            case R.id.button2:
                drawView.clearCanvas();
                break;
        }
    }
}

package com.example.drawapp01;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import androidx.annotation.Nullable;

import java.util.ArrayList;

public class DrawView extends View {
    private float x;
    private float y;
    private Paint paint;
    private Path path;
    private ArrayList<Path> pathList;
    private ArrayList<Float> widthInfo;

    public DrawView(Context context) {
        super(context);
        init();
    }

    public DrawView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DrawView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init(){
        pathList = new ArrayList<Path>();

        paint = new Paint();
        paint.setAntiAlias(true);
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(80.0F);
        paint.setStrokeJoin(Paint.Join.ROUND);

    }

    public void onDraw(Canvas canvas){
        if(path != null){
            canvas.drawPath(path,paint);
        }

        for(Path path:pathList){
            canvas.drawPath(path,paint);
        }
    }

    public boolean onTouchEvent(MotionEvent event){
        x = event.getX();
        y = event.getY();
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN:
                path = new Path();
                path.moveTo(x,y);
                break;
            case MotionEvent.ACTION_MOVE:
                path.lineTo(x,y);
                break;
            case MotionEvent.ACTION_UP:
                float wd = paint.getStrokeWidth();
                path.lineTo(x,y);
                pathList.add(path);
                break;
        }

        invalidate();
        return  true;
    }

    public void backCanvas(){
        path = null;
        int size = pathList.size();
        if(size != 0){
            pathList.remove(size-1);
        }
        invalidate();
    }

    public void clearCanvas(){
        path = null;
        pathList.clear();
        invalidate();
    }

    public void setWidth(float width){
        paint.setStrokeWidth(width);
    }
}
